from turtle import *
from random import randint
import time
import math

def playGame():

    #create turte
    franklin = Turtle()
    franklin.color('red')
    franklin.shape('turtle')
    franklin.hideturtle()

    bob = Turtle()
    bob.color('purple')
    bob.shape('turtle')
    bob.hideturtle()

    tina = Turtle()
    tina.color('green')
    tina.shape('turtle')
    tina.hideturtle()

    
    #code to play game
    showMenu()

    #code to draw title
    drawBoard()


    #draw the turts
    drawTurtles(franklin,bob,tina)

    startRace(franklin,bob,tina)

def showMenu():
    print("WELCOME TO TURTLE RACES!")
    print("Grab a friend (or a few) and guess which color turtle will win the race!")
    print("Red = Franklin")
    print("Blue = Bob")
    print("Green = Tina")
    print()
    print("Ready? (press 'enter' to continue)")
    input()

def drawTitle():
    penup()
    goto(-70,190)
    write("TURTLE RACES",font=("Arial", 20, "normal"))
    
    
def drawBoard():
    #code to draw entire game "board"
    drawTitle()

    speed(10)
    penup()
    goto(-140,140)

    for i in range(15):
        write(i)
        right(90)
        forward(10)
        pendown()
        forward(150)
        penup()
        backward(160)
        left(90)
        forward(20)
    
def drawTurtles(franklin, bob, tina):
    #code to draw turtles

    franklin.showturtle()
    franklin.penup()
    franklin.goto(-160, 100)
    franklin.pendown()

    bob.showturtle()
    bob.penup()
    bob.goto(-160, 50)
    bob.pendown()

    tina.showturtle()
    tina.penup()
    tina.goto(-160, 0)
    tina.pendown()

    

def startRace(franklin, bob, tina):
    #code to do all of the race

    for turn in range(200):
        if franklin.xcor() >= 140:
            penup()
            goto(-90,150)
            color("red")
            write("FRANKLIN WINS")
            break
        
        elif bob.xcor() >= 140:
            penup()
            goto(-90,150)
            color("blue")
            write("Bob WINS")
            break
        
        elif tina.xcor() >= 140:
            penup()
            goto(-90,150)
            color("green")
            write("Tina WINS")
            break

        franklin.forward(randint(1,5))
        tina.forward(randint(1,5))                
        bob.goto(math.cos(turn*(360/200))*50,turn*3)
